/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinedatalib.entities;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author armando
 */
public class DirectorBean {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("cinePU");
    public EntityManager em = emf.createEntityManager();

    public EntityTransaction getTx() {
        if (em != null) {
            return this.em.getTransaction();
        }
        return null;
    }

    public void crear(Director director) throws Exception {
        EntityTransaction tx = getTx();
        try {
            if (em != null) {
                tx.begin(); //Inicio de la transaccion
                em.persist(director); //Inserta los parametros dados
                tx.commit();//Fin de la transaccion
            }
        } catch (Exception e) {
            Logger.getLogger(DirectorBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            tx.rollback(); //corta toda la transaccion si ocurre un error y no guarda nada, además si la PK se deja vacia no guarda la transaccion
        }
    }

    public void update(Director director) throws Exception {
        EntityTransaction tx = getTx();
        try {
            if (em != null) {
                tx.begin();//Inicio de la transaccion
                em.merge(director); //actualiza los parametro dado
                tx.commit();//Fin de la transaccion
            }
        } catch (EntityNotFoundException notFound) {
            Logger.getLogger(DirectorBean.class.getName()).log(Level.SEVERE, notFound.getMessage(), notFound);
            tx.rollback(); //corta toda la transaccion si ocurre un error y no guarda nada, además si la PK se deja vacia no guarda la transaccion
        }
    }

    public void delete(Director director) throws Exception {
        EntityTransaction tx = getTx();
        if (em != null && director != null) {
            try {
                System.out.println("NO SON NULOS");
                tx.begin();
                Director user = em.find(Director.class, director.getIdDirector());
                em.remove(user);
                tx.commit();
            } catch (EntityNotFoundException enfe) {
                Logger.getLogger(DirectorBean.class.getName()).log(Level.SEVERE, enfe.getMessage(), enfe);
                tx.rollback();
            }
        } else {
            System.out.println("NULOS");
        }
    }

    public String formatoNombre(String Nombre) {
        if (Nombre != null) {
            char[] caracteres = Nombre.toCharArray();
            caracteres[0] = Character.toUpperCase(caracteres[0]);
            for (int i = 0; i < Nombre.length() - 2; i++) {
                if (caracteres[i] == ' ' || caracteres[i] == '.' || caracteres[i] == ',') {
                    caracteres[i + 1] = Character.toUpperCase(caracteres[i + 1]);
                }
            }
            Nombre = String.valueOf(caracteres);

        }
        return Nombre;
    }
}
