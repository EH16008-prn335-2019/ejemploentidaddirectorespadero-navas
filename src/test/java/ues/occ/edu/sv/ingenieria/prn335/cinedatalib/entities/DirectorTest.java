/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.cinedatalib.entities;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.mockito.Mockito;

/**
 *
 * @author armando
 */
public class DirectorTest {
    
    public DirectorTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of crear method, of class DirectorBean.
     * @throws java.lang.Exception
     */
    @Test
    public void testCrear() throws Exception{
        System.out.println("crear");
        EntityManager mockEM =Mockito.mock(EntityManager.class);
        EntityTransaction mockTX=Mockito.mock(EntityTransaction.class);
        Mockito.when(mockEM.getTransaction()).thenReturn(mockTX);
        DirectorBean cut = new DirectorBean();
        cut.em=mockEM;
        Director d=new Director();
        d.setNombre("Armando");
        d.setApellido("Herrera");
        d.setActivo(Boolean.TRUE);
        cut.crear(d);
        Mockito.verify(mockEM, Mockito.times(1)).persist(Mockito.any());
    }
    @Test
    public void testEliminar() throws Exception{
        System.out.println("Eliminar");
        EntityManager mockEM =Mockito.mock(EntityManager.class);
        EntityTransaction mockTX=Mockito.mock(EntityTransaction.class);
        Mockito.when(mockEM.getTransaction()).thenReturn(mockTX);
        DirectorBean cut = new DirectorBean();
        cut.em=mockEM;
        Director d=new Director(1);
        cut.delete(d);
        Mockito.verify(mockEM, Mockito.times(1)).remove(Mockito.any());
    }
    @Test
    public void formatoNombreTest()throws Exception{
        System.out.println("FormatoNombre");
        DirectorBean cut = new DirectorBean();
        String valorEsperado="Hola Mundo";
        String Nombre="hola mundo";
        String valorRetornado=cut.formatoNombre(Nombre); 
        assertEquals(valorEsperado, valorRetornado);
    }
    @Test
    public void testUpdate() throws Exception{
        System.out.println("Actualizar");
        EntityManager mockEM= Mockito.mock(EntityManager.class);
        EntityTransaction mockTX = Mockito.mock(EntityTransaction.class);
        Mockito.when(mockEM.getTransaction()).thenReturn(mockTX);
        DirectorBean cut = new DirectorBean();
        cut.em=mockEM;
        Director d = new Director(1,"Armando");
        cut.update(d);
        Mockito.verify(mockEM, Mockito.times(1)).merge(Mockito.any());
    }
    
    
}
    
